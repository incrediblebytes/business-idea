/**
 * v1.0.0
 */
function dm3_clear($) {
	var context = $('.dm3-scroller-inner > .current-content');

	context.find('.flexslider').each(function() {
		$(this).flexslider('destroy');
	});

	context.find('.flexslider-posts .slides > li, .dm3-member-block').off();

	if (!('placeholder' in document.createElement('input'))) {
		context.find('input, textarea').off();
	}

	// Clear gallery functionality.
	context.find('.dm3-gallery').each(function() {
		var gallery = $(this);
		var isotope = gallery.data('isotope');

		if (isotope) {
			isotope.destroy();
		}

		gallery.find('> li').off();
		gallery.prev('.dm3-gallery-terms').find('a').unbind('click');
	});

	// Clear back to top.
	context.find('#footer-back-to-top').off();

	// Reset google maps count.
	if ( typeof dm3_gmap_reset_count === 'function' ) {
		dm3_gmap_reset_count();
	}

	// Clear tabs.
	context.find('.dm3-tabs').each(function() {
		var dm3Tabs = $(this).data('dm3Tabs');

		if (dm3Tabs) {
			dm3Tabs.destroy();
		}
	});

	context.find('#dm3-rs').each(function() {
		var dm3rs = $.data(this, 'dm3Rs');
		dm3rs.destroy();
	});

	context.find('a.ajax-link').off();
	context.find('a').off('.dm3ajaxlinks');

	if ($.magnificPopup.instance) {
		if ($.magnificPopup.instance.items) {
			$.each($.magnificPopup.instance.items, function() {
				if (this.img) {
					this.img.off('.mfploader');
				}
			});
		}

		$.magnificPopup.instance.close();
		$.magnificPopup.instance = null;
	}

	if ($.fn.ajaxFormUnbind) {
		$('div.wpcf7 > form', context).ajaxFormUnbind();
	}

	$.event.trigger({
		type: 'dm3Clear',
		context: context
	});
}

/**
 * Initialize page js before it gets visible
 *
 * Called on page load and everytime we load a page through AJAX (before we show the loaded content)
 * IMPORTANT: use "context" variable in the calls to jQuery inside this function,
 * such that it's new call doesn't conflict with the old one (e.g. when we first load the page) + performance optimization
 */
function dm3_page_initialize($, context) {
	"use strict";

	/**
	 * Dm3Rs Slider
	 */
	$('#dm3-rs', context).dm3Rs();

	/**
	 * Flexslider
	 */
	$('.flexslider', context).each(function() {
		var slider = $(this);
		var args = null;

		if (slider.hasClass('flexslider-posts')) {
			args = {
				animation: 'slide',
				animationLoop: false,
				slideshow: false,
				itemWidth: 220,
				itemMargin: 20,
				prevText: '<span></span>',
				nextText: '<span></span>'
			};
		} else if (slider.hasClass('flexslider-logos')) {
			args = {
				animation: 'slide',
				animationLoop: false,
				slideshow: false,
				itemWidth: 220,
				itemMargin: 20,
				prevText: '<span></span>',
				nextText: '<span></span>'
			};
		} else {
			args = {
				animation: 'slide',
				easing: 'easeInOutExpo',
				slideshow: false,
				prevText: '<span></span>',
				nextText: '<span></span>',
				smoothHeight: true,
				video: true
			};

			var autoscroll = parseInt(slider.data('autoscroll'), 10);

			if (!isNaN(autoscroll) && autoscroll > 0) {
				args.slideshow = true;
				args.slideshowSpeed = autoscroll * 1000;
			}

			var animation = slider.data('animation');

			if (animation) {
				args.animation = animation;
				args.easing = 'swing';
			}
		}

		if (slider.hasClass('direction-nav-hidden')) {
			args.directionNav = false;
		}

		imagesLoaded(slider[0], function() {
			slider.flexslider(args);
		});
	});

	/**
	 * Portfolio (Gallery)
	 */
	$('.dm3-gallery', context).each(function() {
		var gallery = $(this);

		// Isotope
		gallery.css('opacity', 0);

		imagesLoaded(gallery[0], function() {
			gallery.isotope({
				itemSelector: 'li',
				layoutMode: 'fitRows'
			});

			gallery.stop().animate({opacity: 1}, {duration: 500});
		});

		// Magnific Popup
		gallery.magnificPopup({
			delegate: '.mfp-image, .mfp-iframe',
			type: 'image',
			titleSrc: 'title',
			gallery: {
				enabled: false
			}
		});

		gallery.find('> li').hover(function() {
			var li = $(this);
			var desc = li.find('.dm3-gallery-popover:first');
			desc.stop().css({opacity: 0, display: 'block'}).animate({opacity: 1}, {duration: 200});
		}, function() {
			var li = $(this);
			var desc = li.find('.dm3-gallery-popover:first');
			desc.stop().animate({opacity: 0}, {duration: 200, complete: function() {
				$(this).css('display', 'none');
			}});
		});

		// Terms filter
		var terms = gallery.prev('.dm3-gallery-terms');
		terms.find('a').on('click', function(e) {
			var a = $(this);
			var filter = a.data('filter');
			e.preventDefault();
			gallery.isotope({
				filter: filter
			});
			a.parent().siblings().removeClass('active');
			a.parent().addClass('active');
		});
	});

	/**
	 * Flexslider posts
	 */
	$('.flexslider-posts .slides > li, .dm3-member-block').hover(function() {
		$(this).find('> .image, > .dm3-member-image').stop().animate({opacity: 0.6}, {duration: 300});
	}, function() {
		$(this).find('> .image, > .dm3-member-image').stop().animate({opacity: 1}, {duration: 300});
	});

	/**
	 * Shortcodes
	 */
	if (typeof dm3_shortcodes_init === 'function' && context !== null) {
		dm3_shortcodes_init(context);
	}

	/**
	 * Footer
	 */
	$('#footer-back-to-top', context).on('click', function(e) {
		e.preventDefault();
		$('body, html').animate({scrollTop: 0}, {duration: 500, easing: 'easeOutExpo'});
	});

	/**
	 * Magnific popup
	 */
	$('a.dm3-lightbox, .gallery-icon > a', context).magnificPopup({
		type: 'image',
		titleSrc: 'title',
		gallery: {
			enabled: false
		}
	});

	/**
	 * Placeholders in ie
	 */
	if (!('placeholder' in document.createElement('input'))) {
		$('input, textarea', context).each(function() {
			var _this = $(this);
			var placeholder_val = _this.attr('placeholder');

			if (placeholder_val && !_this.val()) {
				_this.addClass('input-placeholder');
				_this.val(placeholder_val);
			}

			_this.on('focus', function() {
				if (_this.val() === placeholder_val) {
					_this.val('');
					_this.removeClass('input-placeholder');
				}
			});

			_this.on('blur', function() {
				if (!_this.val()) {
					_this.val(placeholder_val);
					_this.addClass('input-placeholder');
				}
			});
		});
	}

	/**
	 * Magnific popup
	 */
	$('a.popup-video').magnificPopup({
		type: 'iframe',
		titleSrc: 'title'
	});

	/**
	 * Media type popover
	 */
	$('.media-popover').hover(function() {
		$(this).find('.dm3-gallery-popover:first').stop().css({opacity: 0, display: 'block'}).animate({opacity: 1}, {duration: 200});
	}, function() {
		$(this).find('.dm3-gallery-popover:first').stop().animate({opacity: 0}, {duration: 200, complete: function() {
			$(this).css('display', 'none');
		}});
	});

	/**
	 * Enable twitter
	 */
	if (typeof twttr === 'undefined') {
		!function(d,s,id){
			var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
			if(!d.getElementById(id)){
				js=d.createElement(s);
				js.id=id;
				if (context === null) {
					js.onload = function() {
						twttr.widgets.load();
					};
				}
				js.src=p+"://platform.twitter.com/widgets.js";
				fjs.parentNode.insertBefore(js,fjs);
			}
		}(document,"script","twitter-wjs");
	} else {
		twttr.widgets.load();
	}

	if (context !== null) {
		// Trigger contact form 7.
		$('div.wpcf7 > form', context).wpcf7InitForm();
	}
}

jQuery(window).load(function() {
	dm3_page_initialize(jQuery, null);
});
