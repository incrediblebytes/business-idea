/**
 * dm3Tabs jQuery Plugin
 * version 2.1
 */
(function($) {

	'use strict';

	var count = 0;

	/**
	 * Constructor
	 *
	 * @param {object} el
	 * @param {object} options
	 */
	function Dm3Tabs( el, options ) {
		this.speed = options.speed;
		this.animating = false;
		this.container = $(el);
		this.wrapper = this.container.parent();
		this.tabsContainers = this.container.children('.dm3-tab');
		this.tabsNav = null;
		this.type = 'horizontal';
		this.afterTabChange = options.afterTabChange;
		this.autoscroll = options.autoscroll;
		this.ascTimeout = null;
		this.id = ++count;

		this.preloadImages(10);
	}

	/**
	 * Initialize.
	 */
	Dm3Tabs.prototype.init = function() {
		// Horizontal or vertical.
		if (this.wrapper.hasClass('dm3-tabs-vertical')) {
			this.type = 'vertical';
		}

		// Setup navigation.
		this.setupNav();

		// Just one tab.
		if (this.tabsNav.length < 2) {
			this.wrapper.addClass('dm3-tabs-single');
			return;
		}

		// Setup autoscroll.
		this.setupAutoscroll();

		var that = this;

		if (this.type === 'vertical') {
			var tabsNavHeight = this.tabsNav.eq(0).parent().height();
			var containerBorderWidth = parseInt(this.container.css('borderWidth'), 10);
			this.container.css('min-height', (tabsNavHeight - (containerBorderWidth * 2)) + 'px');
		}

		this.tabsContainers.each(function(i) {
			var tab = $(this);
			var tabCSS = {
				position: 'absolute',
				left: 0,
				width: '100%'
			};

			if (i !== that.getCurrentTab()) {
				tabCSS.display = 'none';
				tabCSS.opacity = 0;
			} else {
				tabCSS.display = 'block';
				tabCSS.opacity = 1;
			}

			tab.css(tabCSS);
		});

		var containerHeight = this.tabsContainers.eq(this.getCurrentTab()).outerHeight(true);
		this.container.css({height: containerHeight + 'px'});

		$(window).on('resize.dm3tabs' + this.id, function() {
			that.onResize();
		});
	};

	Dm3Tabs.prototype.onResize = function() {
		if (this.animating) {
			return;
		}

		this.container.css({
			height: this.tabsContainers.eq(this.getCurrentTab()).outerHeight(true) + 'px'
		});
	};

	/**
	 * Preload images for proper height calculation.
	 *
	 * @param {number} numTries
	 */
	Dm3Tabs.prototype.preloadImages = function(numTries) {
		var loaded = false,
			images = this.container.find('img'),
			that,
			img;

		if (images.length) {
			for (var i = 0; i < images.length; ++i) {
				img = images.eq(i).get(0);

				if (img.complete || img.readyState === 4 || img.readyState === 'complete') {
					loaded = true;
					break;
				}
			}
		} else {
			loaded = true;
		}

		if (loaded || numTries <= 0 ) {
			this.init();
		} else {
			that = this;

			setTimeout(function() {
				that.preloadImages(--numTries);
			}, 300);
		}
	};

	/**
	 * Get the current tab index.
	 *
	 * @return {number}
	 */
	Dm3Tabs.prototype.getCurrentTab = function() {
		return this.tabsNav.filter('.active').index();
	};

	/**
	 * Get the next tab index.
	 *
	 * @return {number}
	 */
	Dm3Tabs.prototype.getNextTab = function() {
		var next = this.getCurrentTab() + 1;

		if (next >= this.tabsContainers.length) {
			next = 0;
		}

		return next;
	};

	/**
	 * Setup navigation.
	 */
	Dm3Tabs.prototype.setupNav = function() {
		var that = this;

		if (this.container.data('navid')) {
			this.tabsNav = $('#' + this.container.data('navid')).children('li');
		} else {
			this.tabsNav = this.wrapper.find('> ul > li');
		}

		this.tabsNav.eq(0).addClass('active');

		this.tabsNav.find('a').on( 'click', function(e) {
			e.preventDefault();
			that.changeTab($(this).parent().index());
		});
	};

	/**
	 * Setup autoscroll.
	 */
	Dm3Tabs.prototype.setupAutoscroll = function() {
		var that = this;
		var autoscroll = parseInt(this.wrapper.data('autoscroll'), 10);

		if (!isNaN(autoscroll)) this.autoscroll = autoscroll;

		if (this.autoscroll) {
			this.wrapper.hover(function() {
				clearTimeout(that.ascTimeout);
			}, function() {
				if (that.autoscroll) that.startAutoscroll();
			});

			that.startAutoscroll();
		}
	};

	/**
	 * Start autoscroll.
	 */
	Dm3Tabs.prototype.startAutoscroll = function() {
		var that = this;

		if ( this.ascTimeout ) {
			clearTimeout(this.ascTimeout);
			this.ascTimeout = null;
		}

		this.ascTimeout = setTimeout(function() {
			that.changeTab(that.getNextTab());
		}, this.autoscroll * 1000);
	};

	/**
	 * Change tab.
	 */
	Dm3Tabs.prototype.changeTab = function(idx) {
		if (this.animating || idx === this.getCurrentTab()) {
			return;
		}

		this.animating = true;

		var nextTab = this.tabsContainers.eq(idx);
		var currentTab = this.tabsContainers.eq(this.getCurrentTab());
		var that = this;
		var containerHeight = nextTab.outerHeight(true);

		// Update navigation.
		this.tabsNav.filter('.active').removeClass('active');
		this.tabsNav.eq(idx).addClass('active');

		// Hide current tab.
		currentTab.stop().animate({opacity: 0}, {duration: this.speed, complete: function() {
			$(this).css({display: 'none'});
		}});

		// Show next tab.
		nextTab.stop().css('display', 'block').animate({opacity: 1}, {duration: this.speed, complete: function() {
			if (that.afterTabChange) {
				that.afterTabChange(that.tabsContainers.eq(that.getCurrentTab()));
			}
		}});

		this.container.stop().animate({height: containerHeight + 'px'}, {duration: this.speed, complete: function() {
			that.animating = false;

			// Autoscroll.
			if (that.autoscroll) that.startAutoscroll();
		}});
	};

	Dm3Tabs.prototype.destroy = function() {
		$(window).off('.dm3tabs' + this.id);
		this.autoscroll = 0;
		clearTimeout(this.ascTimeout);
		this.wrapper.off();
		this.tabsNav.find('a').off();
		$.data(this.container.get(0), 'dm3Tabs', null);
	};

	$.fn.dm3Tabs = function(options) {
		options = $.extend({
			speed: 300,
			afterTabChange: null,
			autoscroll: 0 // in seconds
		}, options);

		return this.each(function() {
			var dm3Tabs = new Dm3Tabs( this, options );
			$.data(this, 'dm3Tabs', dm3Tabs);
		});
	};

}(jQuery));

/**
 * dm3Collapse
 * version 1.0
 */
(function($) {

	'use strict';

	/**
	 * Constructor.
	 */
	function Dm3Collapse(el, options) {
		var that = this;
		this.el = $(el);
		this.options = options;
		this.container = this.el.parent();
		this.transitioning = false;

		this.el.parent().find('.dm3-collapse-trigger > a').on('click', function(e) {
			e.preventDefault();
			if (that.el.hasClass('dm3-in')) {
				that.hide();
			} else {
				that.show();
			}
		});

		if (this.el.hasClass('dm3-in')) {
			this.el.parent().addClass('dm3-collapse-open');
			this.el.removeClass('dm3-collapse');
			this.el.height(this.el.find('> .dm3-collapse-inner').outerHeight());
			setTimeout(function() {
				that.el.addClass('dm3-collapse');
			}, 0);
		}
	}

	/**
	 * Check if browser supports transition end event.
	 *
	 * @return {string}
	 */
	Dm3Collapse.prototype.transitionEnd = function() {
		var el = document.createElement('dm3collapse');
		var transition_end = null;
		var trans_event_names = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd otransitionend',
			'transition': 'transitionend'
		};
		var name;

		for (name in trans_event_names) {
			if (el.style[name] !== undefined) {
				transition_end = trans_event_names[name];
				break;
			}
		}

		return transition_end;
	}();

	/**
	 * Get collapse siblings (for accordion feature).
	 *
	 * @return jQuery
	 */
	Dm3Collapse.prototype.getActives = function() {
		var actives = null;
		var parent = this.container.parent();

		if (parent.length && parent.hasClass('dm3-accordion')) {
			actives = parent.find('> .dm3-collapse-item > .dm3-in');
		}

		return actives;
	};

	/**
	 * Reset the height of the collapse element.
	 *
	 * @param {number} height
	 */
	Dm3Collapse.prototype.reset = function(height) {
		height = (height === null) ? 'auto' : height;
		this.el.removeClass('dm3-collapse');
		this.el.height(height)[0].innerWidth;
		this.el.addClass('dm3-collapse');
	};

	/**
	 * Expand collapsed element.
	 */
	Dm3Collapse.prototype.show = function() {
		if (this.transitioning) {
			return;
		}

		this.transitioning = true;

		this.el.parent().addClass('dm3-collapse-open');

		var that = this;
		var actives = this.getActives();
		var actives_data;

		if (actives) {
			actives_data = actives.data('dm3Collapse');
			if (actives_data && actives_data.transitioning) { return; }
			actives.dm3Collapse('hide');
		}

		var height = this.el.find('> .dm3-collapse-inner').outerHeight();
		var complete = function() {
			that.reset();
			that.transitioning = false;
		};

		this.el.addClass('dm3-in');
		this.el.height(0);

		if (this.transitionEnd) {
			this.el.one(this.transitionEnd, complete);
		} else {
			complete();
		}

		this.el.height(height);
	};

	/**
	 * Collapse the visible element.
	 */
	Dm3Collapse.prototype.hide = function() {
		if (this.transitioning) {
			return;
		}

		this.transitioning = true;

		this.el.parent().removeClass('dm3-collapse-open');
		
		var that = this;
		var height = this.el.find('> .dm3-collapse-inner').outerHeight();
		var complete = function() {
			that.transitioning = false;
		};
		
		this.reset(height);
		this.el.removeClass('dm3-in');
		
		if (this.transitionEnd) {
			this.el.one(this.transitionEnd, complete);
		} else {
			complete();
		}
		
		this.el.height(0);
	};

	/**
	 * jQuery plugin
	 */
	$.fn.dm3Collapse = function(input) {
		var options = $.extend({
		}, typeof input === 'object' && input);

		return this.each(function() {
			var $this = $(this);
			var dm3_collapse = $this.data('dm3Collapse');

			if (!dm3_collapse) {
				$this.data('dm3Collapse', (dm3_collapse = new Dm3Collapse(this, options)));
			}

			if (typeof input === 'string' && typeof dm3_collapse[input] === 'function') {
				dm3_collapse[input]();
			}
		});
	};

}(jQuery));

/**
 * Google maps manager.
 * v1.1
 */
var dm3_create_google_map = null;
var dm3_gmap_reset_count = null;

(function($) {
	'use strict';

	/**
	 * Dm3GoogleMap Constructor.
	 */
	function Dm3GoogleMap(div, options) {
		this.options = options;
		this.lat_lng = null;
		this.resize_to = null;
		this.map_div = div;

		// Update height if necessary.
		var height = this.map_div.data('height');

		if (height) {
			this.map_div.css({
				height: height + 'px'
			});
		}

		var address = this.map_div.data('address');

		if (address) {
			this.geocode(address);
		} else {
			this.setupLatLng();
			this.generateMap();
		}
	}

	/**
	 * Generate google map.
	 */
	Dm3GoogleMap.prototype.generateMap = function() {
		var that = this;

		if (!this.lat_lng) {
			return;
		}

		var mapOptions = $.extend({
			zoom: 11,
			scrollwheel: false,
			streetViewControl: false,
			zoomControl: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}, this.options.mapOptions);

		mapOptions.center = this.lat_lng;

		this.map = new google.maps.Map(this.map_div.get(0), mapOptions);

		if (this.map.getProjection()) {
			this.offsetMap();
		} else {
			google.maps.event.addListener(this.map, 'projection_changed', function() {
				that.offsetMap();
			});
		}

		var markerOptions = {
			position: this.lat_lng,
			map: this.map
		};

		if (this.options.markerIcon) {
			markerOptions.icon = this.options.markerIcon;
		}

		new google.maps.Marker(markerOptions);

		$(window).on('resize', function() {
			that.resize();
		});
	};

	/**
	 * Geocode an address.
	 *
	 * @param {string} address
	 */
	Dm3GoogleMap.prototype.geocode = function(address) {
		var that = this;
		var geocoder = new google.maps.Geocoder();

		geocoder.geocode({address: address}, function(results, status) {
			if (status === google.maps.GeocoderStatus.OK) {
				that.lat_lng = results[0].geometry.location;
				that.generateMap();
			}
		});
	};

	/**
	 * Setup latitude and longitude.
	 * Takes latitude and longitude from HTML data attributes.
	 */
	Dm3GoogleMap.prototype.setupLatLng = function() {
		var lat = this.map_div.data('latitude');
		var lng = this.map_div.data('longitude');

		if (lat && lng) {
			this.lat_lng = new google.maps.LatLng(lat, lng);
		}
	};

	/**
	 * Center map.
	 */
	Dm3GoogleMap.prototype.offsetMap = function() {
		var offsetx = 0;
		var offsety = 0;
		var point1 = this.map.getProjection().fromLatLngToPoint(this.lat_lng);
		var point2 = new google.maps.Point(offsetx / Math.pow(2, this.map.getZoom()) || 0, offsety / Math.pow(2, this.map.getZoom()) || 0);
		this.map.setCenter(this.map.getProjection().fromPointToLatLng(new google.maps.Point(point1.x - point2.x, point1.y + point2.y)));
	};

	/**
	 * Resize map on window's "resize" event.
	 */
	Dm3GoogleMap.prototype.resize = function() {
		var that = this;

		if (this.resize_to) {
			clearTimeout(this.resize_to);
			this.resize_to = null;
		}

		this.resize_to = setTimeout(function() {
			that.offsetMap();
		}, 500);
	};

	var dm3Pages = {};
	var dm3PageCount = 0;
	var dm3GoogleMapCount = 0;
	var dm3GoogleMapCache = {};

	dm3_gmap_reset_count = function() {
		dm3GoogleMapCount = 0;
	};

	dm3_create_google_map = function(div, options) {
		var gMap;
		var pageID;

		if (typeof dm3Pages[window.location.href] !== 'undefined') {
			pageID = dm3Pages[window.location.href];
		} else {
			pageID = ++dm3PageCount;
			dm3Pages[window.location.href] = pageID;
		}

		var mapID = pageID + '_' + (++dm3GoogleMapCount);

		if (dm3GoogleMapCache[mapID]) {
			gMap = dm3GoogleMapCache[mapID];
			div.replaceWith(gMap.map_div);
			$(window).resize();
		} else {
			options = $.extend({
				markerIcon: null,
				mapOptions: {}
			}, options);
			gMap = new Dm3GoogleMap(div, options);
			dm3GoogleMapCache[mapID] = gMap;
		}
	};
})(jQuery);

/**
 * Initialize shortcodes
 */
var dm3_shortcodes_init = (function($) {
	'use strict';

	return function(context) {
		if (context == undefined) {
			context = null;
		}

		// Tabs.
		$('.dm3-tabs', context).dm3Tabs();

		// Collapse / Accordion.
		$('.dm3-collapse', context).dm3Collapse();

		// Alert boxes.
		$('.dm3-alert', context).each(function() {
			var div_alert = $(this);
			var btn_close = $('<a class="dm3-alert-close" href="#">&times;</a>');
			btn_close.on('click', function(e) {
				e.preventDefault();
				$(this).parent().hide();
			});
			div_alert.append(btn_close);
		});

		// Google maps.
		$('.dm3-google-map', context).each(function() {
			if (typeof google === 'undefined') {
				return;
			}
			
			dm3_create_google_map($(this), {
				mapOptions: {
					zoom: 12,
					mapTypeControl: true,
					mapTypeControlOptions: {style: google.maps.MapTypeControlStyle.DROPDOWN_MENU},
					navigationControl: true,
					navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
				}
			});
		});
	};
}(jQuery));

/**
 * Run plugins
 */
(function($) {
	'use strict';

	$(document).ready(function() {
		dm3_shortcodes_init();
	});
}(jQuery));
