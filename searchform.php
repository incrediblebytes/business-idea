<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<label>
		<span class="screen-reader-text"><?php echo _x( 'Search for:', 'label', 'dm3_fwk' ); ?></span>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'dm3_fwk' ); ?>" value="<?php echo esc_attr( get_search_query() ); ?>" name="s" title="<?php echo _x( 'Search for:', 'label', 'dm3_fwk' ); ?>" />
	</label>
	<button type="submit" class="search-submit"><span class="font-icon-search"></span></button>
</form>
