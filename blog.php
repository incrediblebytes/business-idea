<?php
/**
 * Template Name: Blog
 */

$custom = get_post_custom( get_the_ID() );
$args = array();
$args['posts_per_page'] = get_option( 'posts_per_page' );
$args['paged'] = get_query_var( 'paged' );
$loop = new WP_Query( $args );
?>

<?php get_header(); ?>

<?php if ( ! isset( $custom['dm3_fwk_hide_page_title'] ) || $custom['dm3_fwk_hide_page_title'][0] != 1 ) : ?>
	<section id="content-header">
		<div class="container clearfix">
			<div class="sixteen columns">
				<h1><?php _e( 'Blog', 'dm3_fwk' ); ?></h1>
				<?php echo dm3_page_subtitle( $custom ); ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<section class="section">
	<div class="container clearfix">
		<div class="eleven columns">
			<?php if ( $loop->have_posts() ): ?>
				<?php while ( $loop->have_posts() ): $loop->the_post(); ?>
					<?php get_template_part( 'include/content', get_post_format() ); ?>
				<?php endwhile; ?>

				<div class="pager">
					<?php
						$big = 999999999; // need an unlikely integer

						echo paginate_links( array(
							'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format'  => '?paged=%#%',
							'current' => max( 1, get_query_var( 'paged' ) ),
							'total'   => $loop->max_num_pages,
						) );
					?>
				</div>
				<?php wp_reset_postdata(); ?>
			<?php endif; ?>
		</div>

		<aside class="sidebar five columns">
			<div class="sidebar-inner">
				<?php get_sidebar(); ?>
			</div>
		</aside>
	</div>
</section>

<?php get_footer(); ?>
