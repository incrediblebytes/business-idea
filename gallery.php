<?php
/**
 * Template Name: Gallery
 */

global $post;

// Get post custom fields.
$custom = get_post_custom( $post->ID );
$gallery_columns = isset( $custom['dm3_fwk_columns'] ) ? intval( $custom['dm3_fwk_columns'][0] ) : 3;
$gallery_class = 'dm3-gallery dm3-gallery-' . $gallery_columns;

// Get categories.
$categories = get_terms( array( 'gallery_cat' ), 'objects' );

// Get posts.
$args = array();
$args['post_type'] = 'gallery';
$args['posts_per_page'] = -1;
$loop = new WP_Query( $args );
?>

<?php get_header(); ?>

<?php if ( ! isset( $custom['dm3_fwk_hide_page_title'] ) || $custom['dm3_fwk_hide_page_title'][0] != 1 ) : ?>
	<section id="content-header">
		<div class="container clearfix">
			<div class="sixteen columns">
				<h1><?php the_title(); ?></h1>
				<?php echo dm3_page_subtitle( $custom ); ?>
			</div>
		</div>
	</section>
<?php endif; ?>

<section class="section">
	<div class="container clearfix">
		<div class="sixteen columns">
			<?php if ( have_posts() ) : ?>
				<?php while ( have_posts() ): the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<?php the_content(); ?>
					</article>
				<?php endwhile; ?>
			<?php endif; ?>
			
			<?php if ( $categories ) : ?>
				<ul class="dm3-gallery-terms">
					<li class="active"><a href="#" data-filter="*"><?php _e( 'All', 'dm3_fwk' ); ?></a></li>
					<?php foreach ( $categories as $c ) : ?>
						<li><a href="#" data-filter=".term-<?php echo esc_attr( $c->term_id ); ?>"><?php echo esc_html( $c->name ); ?></a></li>
					<?php endforeach; ?>
				</ul>
			<?php endif; ?>

			<?php
				if ( $loop->have_posts() ) {
					echo '<ul class="' . esc_attr( $gallery_class ) . '">';

					while ( $loop->have_posts() ) {
						$loop->the_post();
						get_template_part( 'include/content', 'gallery' );
					}

					wp_reset_postdata();

					echo '</ul>';
				} else {
					echo '<p>' . __( 'There are no posts in gallery', 'dm3_fwk' ) . '</p>';
				}

				edit_post_link( __( 'Edit', 'dm3_fwk' ), '<p class="edit-link">', '</p>' );
			?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
