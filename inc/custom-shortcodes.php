<?php

add_filter( 'dm3_shortcodes_extra', '__return_false' );

/**
 * Setup the UI for the theme's custom shortcodes.
 *
 * @param array $shortcodes
 * @return array
 */
function dm3sc_extra_config( $shortcodes ) {
	// Get post types.
	$post_types = array();
	$post_types[] = array(
		'label' => __( 'Select post type', 'dm3_fwk' ),
		'value' => '',
	);
	$post_types[] = array(
		'label' => __( 'Posts', 'dm3_fwk' ),
		'value' => 'post',
	);
	$tmp_types = get_post_types( array(
		'_builtin' => false,
	), 'object' );

	foreach( $tmp_types as $type ) {
		$post_types[] = array(
			'label' => $type->labels->name,
			'value' => $type->name,
		);
	}

	// Team member.
	$shortcodes[] = array(
		'label'     => __( 'Team member', 'dm3_fwk' ),
		'shortcode' => '[dm3_team_member image="@image" name="@name" title="@title" twitter="@twitter" facebook="@facebook" skype="@skype" email="@email"]@description[/dm3_team_member]',
		'options'   => array(
			'image'       => array(
				'type'  => 'text',
				'label' => __( 'Image', 'dm3_fwk' ),
			),
			'name'        => array(
				'type'  => 'text',
				'label' => __( 'Name', 'dm3_fwk' ),
			),
			'title'       => array(
				'type'  => 'text',
				'label' => __( 'Title', 'dm3_fwk' ),
			),
			'description' => array(
				'type'  => 'textarea',
				'label' => __( 'Description', 'dm3_fwk' ),
			),
			'twitter'     => array(
				'type'  => 'text',
				'label' => __( 'Twitter URL', 'dm3_fwk' ),
			),
			'facebook'    => array(
				'type'  => 'text',
				'label' => __( 'Facebook URL', 'dm3_fwk' ),
			),
			'skype'       => array(
				'type'  => 'text',
				'label' => __( 'Skype', 'dm3_fwk' ),
			),
			'email'       => array(
				'type'  => 'text',
				'label' => __( 'Email', 'dm3_fwk' ),
			),
		),
	);

	// Price table.
	$shortcodes[] = array(
		'label'           => __( 'Price table', 'dm3_fwk' ),
		'max'             => 5,
		'shortcode'       => '[dm3_price_table]@child_shortcode[/dm3_price_table]',
		'child_shortcode' => array(
			'shortcode'      => '[dm3_price_column title="@title" price="@price" per="@per" url="@url" style="@style" buttontext="@buttontext" buttonclass="@buttonclass"]@options[/dm3_price_column]',
			'addButtonLabel' => __( 'Add another column', 'dm3_fwk' ),
			'options'        => array(
				'title'      => array(
					'type'  => 'text',
					'label' => __( 'Title', 'dm3_fwk' ),
				),
				'price'      => array(
					'type'  => 'text',
					'label' => __( 'Price', 'dm3_fwk' ),
				),
				'per'        => array(
					'type'  => 'text',
					'label' => __( 'Per', 'dm3_fwk' ),
				),
				'options'    => array(
					'type'  => 'textarea',
					'label' => __( 'Options', 'dm3_fwk' ),
				),
				'url'        => array(
					'type'  => 'text',
					'label' => __( 'URL', 'dm3_fwk' ),
				),
				'buttontext' => array(
					'type'  => 'text',
					'label' => __( 'Button text', 'dm3_fwk' ),
				),
				'buttonclass' => array(
					'type'  => 'text',
					'label' => __( 'Custom CSS class for the button', 'dm3_fwk' ),
				),
				'style'       => array(
					'type'    => 'select',
					'label'   => __( 'Style', 'dm3_fwk' ),
					'options' => array(
						array( 'label' => __( 'Default', 'dm3_fwk' ), 'value' => 'default' ),
						array( 'label' => __( 'Featured', 'dm3_fwk' ), 'value' => 'featured' )
					),
				),
			),
		),
	);

	$shortcodes[] = array(
		'label'      => __( 'Carousel', 'dm3_fwk' ),
		'shortcodes' => array(
			// Posts carousel.
			array(
				'label'     => __( 'Posts carousel', 'dm3_fwk' ),
				'shortcode' => '[dm3_posts_carousel posttype="@post_type" category="@category" amount="@amount" excerptlength="@excerptlength" /]',
				'options'   => array(
					'post_type'     => array(
						'type'    => 'select',
						'label'   => __( 'Post type', 'dm3_fwk' ),
						'options' => $post_types,
					),
					'category'      => array(
						'type'    => 'select',
						'label'   => __( 'Category', 'dm3_fwk' ),
						'options' => array(),
					),
					'amount'        => array(
						'type'  => 'text',
						'label' => __( 'Number of posts', 'dm3_fwk' ),
						'value' => 8,
					),
					'excerptlength' => array(
						'type'  => 'text',
						'label' => __( 'Excerpt length', 'dm3_fwk' ),
						'value' => 55,
					),
				),
			),
			// Clients carousel.
			array(
				'label'           => __( 'Clients carousel', 'dm3_fwk' ),
				'shortcode'       => '[dm3_clients]@child_shortcode[/dm3_clients]',
				'child_shortcode' => array(
					'shortcode'      => '[dm3_client clienturl="@clienturl" title="@title"]@imageurl[/dm3_client]',
					'addButtonLabel' => __( 'Add client', 'dm3_fwk' ),
					'options'        => array(
						'imageurl'  => array(
							'type'  => 'text',
							'label' => __( 'Image URL', 'dm3_fwk' ),
						),
						'clienturl' => array(
							'type'  => 'text',
							'label' => __( 'Client URL', 'dm3_fwk' ),
						),
						'title'     => array(
							'type'  => 'text',
							'label' => __( 'Title', 'dm3_fwk' ),
						),
					),
				),
			)
		)
	);

	// Box.
	$shortcodes[] = array(
		'label'     => __( 'Box', 'dm3_fwk' ),
		'shortcode' => '[dm3_box]@content[/dm3_box]',
		'options'   => array(
			'content' => array(
				'type'  => 'textarea',
				'label' => __( 'Content', 'dm3_fwk' ),
			),
		),
	);

	// Google map.
	foreach ( $shortcodes as $key => $shortcode ) {
		if ( isset( $shortcode['shortcode'] ) && strpos( $shortcode['shortcode'], '[dm3_google_map' ) === 0 ) {
			$shortcodes[ $key ] = array(
				'label'     => __( 'Google map', 'dm3_fwk' ),
				'shortcode' => '[dm3_google_map address="@address" height="@height" /]',
				'options'   => array(
					'address' => array(
						'type'  => 'text',
						'label' => __( 'Address', 'dm3_fwk' ),
					),
					'height'  => array(
						'type'  => 'text',
						'label' => __( 'Height (in pixels)', 'dm3_fwk' ),
					),
				),
			);

			break;
		}
	}

	return $shortcodes;
}
add_filter( 'dm3_shortcodes', 'dm3sc_extra_config' );

/**
 * Get taxonomies (ajax).
 */
function dm3sc_get_taxonomies() {
	$post_type = isset( $_POST['post_type'] ) ? $_POST['post_type'] : '';

	if ( $post_type ) {
		$post_type_object = get_post_type_object( $post_type );

		if ( $post_type_object ) {
			if ( is_array( $post_type_object->taxonomies ) && count( $post_type_object->taxonomies ) ) {
				$categories = get_terms( $post_type_object->taxonomies, 'objects' );
			} else {
				$categories = get_categories( array( 'type' => $post_type ) );
			}

			if ( $categories ) {
				$response_categories = array();

				foreach( $categories as $cat ) {
					$response_categories[] = array(
						'label' => $cat->name,
						'value' => $cat->term_id,
					);
				}

				echo json_encode( $response_categories );
				exit();
			}
		}
	}

	echo '';
	exit();
}
add_action( 'wp_ajax_dm3sc_get_taxonomies', 'dm3sc_get_taxonomies' );

function dm3sc_taxonomies_ajax() {
	?>
	<script>
	jQuery(document).ready(function() {
		jQuery('body').on('dm3-shortcodes-open', function() {
			var contentBox = jQuery('div.dm3-content-box-inner');
			var select_post_type = contentBox.find('select[name=post_type]');

			select_post_type.on('change', function() {
				jQuery.ajax({
					type: 'post',
					url: ajaxurl,
					cache: false,
					dataType: 'json',
					data: {
						action: 'dm3sc_get_taxonomies',
						post_type: select_post_type.val()
					},
					success: function(response) {
						var select_category = jQuery('div.dm3-content-box-inner select[name=category]').html('');

						select_category.append('<option value=""><?php _e( 'Any category', 'dm3_fwk' ); ?></option>');

						for (var i = 0; i < response.length; ++i) {
							select_category.append('<option value="' + response[i].value + '">' + response[i].label + '</option>');
						}
					}
				});
			});
		});
	});
	</script>
	<?php
}
add_action( 'admin_head', 'dm3sc_taxonomies_ajax' );

// SHORTCODES:

if ( ! function_exists( 'dm3sc_shortcode_price_table' ) ) :
/**
 * Price table shortcode.
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function dm3sc_shortcode_price_table( $atts, $content = null ) {
	$classes = 'dm3-pricing';
	$num_columns = intval( substr_count( $content, '[dm3_price_column' ) );
	$classes .= ' dm3-pricing-' . $num_columns;

	return '<div class="' . esc_attr( $classes ) . '">' . dm3sc_do_shortcode( $content ) . '</div>';
}
add_shortcode( 'dm3_price_table', 'dm3sc_shortcode_price_table' );
endif;

if ( ! function_exists( 'dm3sc_shortcode_price_column' ) ) :
/**
 * Price table column shortcode.
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function dm3sc_shortcode_price_column( $atts, $content = null ) {
	$atts = shortcode_atts( array(
		'title'       => '',
		'price'       => '',
		'per'         => '',
		'url'         => '',
		'buttontext'  => '',
		'buttonclass' => '',
		'style'       => 'default',
	), $atts );

	$output = '<div class="dm3-pricing-column' . ( $atts['style'] == 'featured' ? ' dm3-pricing-featured' : '' ) . '">';
	$output .= '<div class="dm3-pricing-header"><h2>' . esc_html( $atts['title'] ) . '</h2></div>';
	$output .= '<div class="dm3-pricing-price"><span>' . esc_html( $atts['price'] ) . '</span> <i>' . esc_html( $atts['per'] ) . '</i></div>';
	$output .= '<div class="dm3-pricing-options">' . dm3sc_content( $content ) . '</div>';
	$buttonclass = ! empty( $atts['buttonclass'] ) ? $atts['buttonclass'] : 'dm3-pricing-button';
	$output .= '<div class="dm3-pricing-actions"><a' . ' class="' . esc_attr( $buttonclass ) . '" href="' . esc_url( $atts['url'] ) . '">' . esc_html( $atts['buttontext'] ) . '</a></div>';
	$output .= '</div>';

	return $output;
}
add_shortcode( 'dm3_price_column', 'dm3sc_shortcode_price_column' );
endif;

if ( ! function_exists( 'dm3sc_shortcode_team_member' ) ) :
/**
 * Team member.
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function dm3sc_shortcode_team_member( $atts, $content = null ) {
	$atts = shortcode_atts( array(
		'image'    => '',
		'name'     => '',
		'title'    => '',
		'twitter'  => '',
		'facebook' => '',
		'skype'    => '',
		'email'    => '',
	), $atts );

	$output = '<div class="dm3-member-block">';
	
	// Image
	if ( $atts['image'] ) {
		$output .= '<div class="dm3-member-image"><img src="' . esc_url( $atts['image'] ) . '" alt=""></div>';
	}

	// Name and title
	$output .= '<div class="dm3-member-title">';
	$output .= '<h2>' . esc_html( $atts['name'] ) . '</h2>';
	if ( $atts['title'] ) {
		$output .= '<p>' . esc_html( $atts['title'] ) . '</p>';
	}
	$output .= '</div>';

	// Description
	$output .= '<div class="dm3-member-description">' . wp_kses( $content, array( 'a' => array( 'href' => array(), 'target' => array(), 'title' => array() ) ) ) . '</div>';

	// Social
	if ( $atts['twitter'] || $atts['facebook'] || $atts['skype'] || $atts['email'] ) {
		$output .= '<div class="dm3-member-social">';

		if ( $atts['twitter'] ) {
			$output .= '<a class="dm3-member-twitter" href="' . esc_url( $atts['twitter'] ) . '" title="' . __( 'Twitter', 'dm3_fwk' ) . '"><span class="font-icon-twitter"></span></a>';
		}

		if ( $atts['facebook'] ) {
			$output .= '<a class="dm3-member-facebook" href="' . esc_url( $atts['facebook'] ) . '" title="' . __( 'Facebook', 'dm3_fwk' ) . '"><span class="font-icon-facebook"></span></a>';
		}

		if ( $atts['skype'] ) {
			$output .= '<a class="dm3-member-skype" href="' . esc_url( $atts['skype'] ) . '" title="' . __( 'Skype', 'dm3_fwk' ) . '"><span class="font-icon-skype"></span></a>'; 
		}

		if ( $atts['email'] ) {
			$output .= '<a class="dm3-member-email" href="' . esc_url( $atts['email'] ) . '" title="' . __( 'Email', 'dm3_fwk' ) . '"><span class="font-icon-envelope"></span></a>'; 
		}

		$output .= '</div>';
	}

	$output .= '</div>';

	return $output;
}
add_shortcode( 'dm3_team_member', 'dm3sc_shortcode_team_member' );
endif;

if ( ! function_exists( 'dm3sc_shortcode_posts_carousel' ) ) :
/**
 * Posts carousel.
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function dm3sc_shortcode_posts_carousel( $atts, $content = null ) {
	$atts = shortcode_atts( array(
		'posttype'      => '',
		'category'      => '',
		'amount'        => 1,
		'excerptlength' => 55,
	), $atts );

	// Get posts
	global $post;

	$params = array(
		'post_type' => $atts['posttype'],
		'showposts' => $atts['amount'],
		'orderby'   => 'id',
		'order'     => 'DESC',
	);

	if ( is_numeric( $atts['category'] ) ) {
		if ( 'post' == $atts['posttype'] ) {
			$params['cat'] = $atts['category'];
		} else {
			$taxonomy = get_object_taxonomies( $atts['posttype'], 'names' );
			$taxonomy = isset( $taxonomy[0] ) ? $taxonomy[0] : null;

			if ( $taxonomy ) {
				$params['tax_query'] = array(
					array(
						'taxonomy' => $taxonomy,
						'field'    => 'id',
						'terms'    => array( $atts['category'] ),
					),
				);
			}
		}
	}

	$output = apply_filters( 'dm3sc_posts_carousel', '<ul>', 'before posts' );

	$query = new WP_Query( $params );

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
			$has_image = current_theme_supports( 'post-thumbnails' ) && has_post_thumbnail();
			$permalink = get_permalink();
			$title = get_the_title();
			$output .= apply_filters( 'dm3sc_posts_carousel', '<li>', 'before post' );

			// Image.
			if ( $has_image ) {
				$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'gallery' );

				if ( is_array( $thumb ) && isset( $thumb[0] ) ) {
					$thumb = $thumb[0];
				}

				$output .= '<a class="image" href="' . esc_url( $permalink ) . '" title="' . $title . '"><img src="' . esc_url( $thumb ) . '" alt=""></a>';
			}

			// Description.
			$output .= '<div class="description">';

			// Title.
			$output .= '<h2><a href="' . esc_url( $permalink ) . '">' . $title . '</a></h2>';

			// Excerpt.
			$excerpt = get_the_excerpt();

			if ( strlen( $excerpt ) > $atts['excerptlength'] ) {
				$excerpt = substr( $excerpt, 0, intval( $atts['excerptlength'] ) );

				// Fix sliced last word problem.
				$pos = strrpos( $excerpt, ' ' );

				if ( $pos > 0 ) {
					$excerpt = substr( $excerpt, 0, $pos );
				}

				$output .= '<p>' . $excerpt . '&hellip;</p>';
			} else {
				$output .= '<p>' . $excerpt . '</p>';
			}

			$output .= '</div>';
			$output .= apply_filters( 'dm3sc_posts_carousel', '</li>', 'after post' );
		}

		wp_reset_postdata();
	}

	$output .= apply_filters( 'dm3sc_posts_carousel', '</ul>', 'after posts' );

	return $output;
}
add_shortcode( 'dm3_posts_carousel', 'dm3sc_shortcode_posts_carousel' );
endif;

if ( ! function_exists( 'dm3_shortcode_clients' ) ) :
/**
 * Clients carousel wrapper.
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function dm3_shortcode_clients( $atts, $content = null ) {
	$output = apply_filters( 'dm3sc_clients_carousel', '<ul class="dm3-clients">', 'before' );
	$output .= dm3sc_do_shortcode( $content );
	$output .= apply_filters( 'dm3sc_clients_carousel', '</ul>', 'after' );

	return $output;
}
add_shortcode( 'dm3_clients', 'dm3_shortcode_clients' );
endif;

if ( ! function_exists( 'dm3_shortcode_client' ) ) :
/**
 * Clients carousel client.
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function dm3_shortcode_client( $atts, $content = null ) {
	$atts = shortcode_atts( array(
		'clienturl' => '',
		'title'     => '',
	), $atts );

	$output = '<li>';

	if ( $atts['clienturl'] ) {
		$output .= '<a href="' . esc_url( $atts['clienturl'] ) . '"';

		if ( $atts['title'] ) {
			$output .= ' title="' . esc_attr( $atts['title'] ) . '"';
		}

		$output .= ' target="_blank">';
	}

	$output .= '<img src="' . esc_url( $content ) . '" alt="">';

	if ( $atts['clienturl'] ) {
		$output .= '</a>';
	}

	$output .= '</li>';

	return $output;
}
add_shortcode( 'dm3_client', 'dm3_shortcode_client' );
endif;

if ( ! function_exists( 'dm3sc_shortcode_box' ) ) :
/**
 * Box shortcode.
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function dm3sc_shortcode_box( $atts, $content = null ) {
	return '<div class="dm3-box">' . dm3sc_do_shortcode( $content ) . '</div>';
}
add_shortcode( 'dm3_box', 'dm3sc_shortcode_box' );
endif;

if ( ! function_exists( 'dm3sc_shortcode_google_map' ) ) :
/**
 * Google map shortcode.
 *
 * @param array $atts
 * @param string $content
 * @return string
 */
function dm3sc_shortcode_google_map( $atts, $content = null ) {
	static $map_id = 0;

	$map_id += 1;

	$atts = shortcode_atts( array(
		'address' => '',
		'height'  => '',
	), $atts );

	if ( ! $atts['height'] ) {
		$atts['height'] = 300;
	}

	$output = '<div id="dm3-google-map-' . $map_id . '" class="dm3-google-map" data-height="' . intval( $atts['height'] ) . '" data-address="' . esc_attr( $atts['address'] ) . '"></div>';
	$output .= '<script>if (!window.google || !window.google.maps) {document.write(\'<\' + \'script src="//maps.google.com/maps/api/js?sensor=false"\' + \' type="text/javascript"><\' + \'/script>\');}</script>';

	return $output;
}
add_shortcode( 'dm3_google_map', 'dm3sc_shortcode_google_map' );
endif;
