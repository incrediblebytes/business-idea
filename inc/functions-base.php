<?php

if ( ! function_exists( 'dm3_option' ) ) :
/**
 * Get the default value for a theme option.
 *
 * @param string $key
 * @param mixed $default_value
 * @return mixed
 */
function dm3_option( $key, $default_value = null ) {
	return $default_value;
}
endif;

/**
 * Get the theme's image sizes.
 *
 * @return array
 */
function dm3_get_img_sizes() {
	$dm3_img_size = array();
	$dm3_img_size['slider1'] = array( 'w' => 940, 'h' => 380, 'name' => __( 'Slider 1', 'dm3_fwk' ) );
	$dm3_img_size['slider2'] = array( 'w' => 640, 'h' => 320, 'name' => __( 'Slider 2', 'dm3_fwk' ) );
	$dm3_img_size['gallery'] = array( 'w' => 470, 'h' => 320, 'name' => __( 'Gallery items', 'dm3_fwk' ) );
	$dm3_img_size['gallery_single'] = array( 'w' => 614, 'h' => 420, 'name' => __( 'Gallery single', 'dm3_fwk' ) );
	$dm3_img_size['blog'] = array( 'w' => 640, 'h' => 320, 'name' => __( 'Blog', 'dm3_fwk' ) );
	$dm3_img_size['widget'] = array( 'w' => 50, 'h' => 50, 'name' => __( 'Widget', 'dm3_fwk' ) );

	return $dm3_img_size;
}

if ( ! function_exists( 'dm3_get_video' ) ) :
/**
 * Get video HTML.
 *
 * @param string $url
 * @param int $width In pixels.
 * @param int $height In pixels.
 *
 * @return string
 */
function dm3_get_video( $url, $width = 940, $height = 360 ) {
	$output = '';

	if ( preg_match( '/^(https?:\/\/)?(www\.)?youtu(\.be|be\.com)+\/(.+\=)?([^&?#]+)/i', $url, $matches ) ) {
		// Youtube.
		$output = '<iframe width="' . intval( $width ) . '" height="' . intval( $height ) . '" src="http://www.youtube.com/embed/' . esc_attr( $matches[5] ) . '?wmode=transparent" frameborder="0" allowfullscreen></iframe>';
	} elseif ( preg_match( '/^https?:\/\/vimeo\.com\/([0-9]+)/i', $url, $matches ) ) {
		// Vimeo.
		$output = '<iframe src="http://player.vimeo.com/video/' . esc_attr( $matches[1] ) . '?title=0&amp;byline=0&amp;portrait=0&amp;color=ffffff" width="' . intval( $width ) . '" height="' . intval( $height ) . '" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
	}

	return $output;
}
endif;

if ( ! function_exists( 'dm3_include_slideshow' ) ) :
/**
 * Include slideshow.
 *
 * @param int $post_id
 * @param array $custom
 * @param array $args
 */
function dm3_include_slideshow( $post_id, $custom, $args = array() ) {
	$args = array_merge( array(
		'before' => '',
		'after'  => '',
		'w'      => null,
		'h'      => null
	), $args );

	if ( isset( $custom['dm3_fwk_slideshow'] ) ) {
		switch ( $custom['dm3_fwk_slideshow'][0] ) {
			case 'none':
				break;

			case 'slider-1':
				include get_template_directory() . '/include/slider-1.php';
				break;

			case 'slider-2':
				include get_template_directory() . '/include/slider-2.php';
				break;
		}
	}
}
endif;

if ( ! function_exists( 'dm3_comment' ) ) :
/**
 * Output a comment (for wp_list_comments function).
 *
 * @param Object $comment
 * @param array $args
 * @param int $depth
 */
function dm3_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	$output = '';
	$edit_comment_link = get_edit_comment_link();

	if ( $edit_comment_link ) {
		$edit_comment_link = '<a class="comment-edit-link" href="' . esc_url( $edit_comment_link ) . '" title="' . __( 'Edit comment', 'dm3_fwk' ) . '">' . __( 'Edit', 'dm3_fwk' ) . '</a>';
	}

	switch ( $comment->comment_type ) {
		case '':
			$comment_classes = implode( ' ', get_comment_class() );
			$output .= '<li class="' . $comment_classes . '" id="li-comment-' . get_comment_ID() . '">';
			$output .= '<div class="comment-wrap clearfix" id="comment-' . get_comment_ID() . '">';

			// Avatar
			$output .= get_avatar( $comment, 40 );

			// Comment meta
			$output .= '<div class="comment-meta">';
			$output .= '<span class="comment-author"><i>' . __( 'by', 'dm3_fwk' ) . '</i> ' . get_comment_author_link() . '</span>';
			$output .= ' <span class="comment-date"><i>' . __( 'on', 'dm3_fwk' ) . '</i> ';
			/* translators: 1: date, 2: time */
			$output .= sprintf( __( '%1$s at %2$s', 'dm3_fwk' ), get_comment_date(), get_comment_time() );
			$output .= '</span>';

			if ( $edit_comment_link ) {
				$output .= ' <span> | ' . $edit_comment_link . '</span>';
			}

			$output .= '</div>';

			$output .= '<div class="comment-body">';

			// Approved
			if ( $comment->comment_approved == '0' ) {
				$output .= '<p><em class="comment-awaiting-moderation">' . __( 'Your comment is awaiting moderation.', 'dm3_fwk' ) . '</em></p>';
			}

			// Comment text
			$output .= get_comment_text();

			// Reply link
			$output .= '<p class="comment-reply">' . get_comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ) . '</p>';

			$output .= '</div></div>';
			break;

		case 'pingback':
		case 'trackback':
			$output .= '<li class="post pingback">';
			$output .= '<p>' . __( 'Pingback:', 'dm3_fwk' ) . ' ' . get_comment_author_link() . ' ' . $edit_comment_link . '</p>';
		break;
	}

	echo $output;
}
endif;

if ( ! function_exists( 'dm3_post_meta' ) ) :
/**
 * Output post meta.
 *
 * @return string
 */
function dm3_post_meta() {
	$output = '<div class="post-meta"><ul>';

	$output .= '<li><span class="post-meta-label">' . __( 'Posted on', 'dm3_fwk' ) . '</span> ';
	$posted_on = sprintf( '<a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date published" datetime="%3$s">%4$s</time></a>',
		esc_url( get_permalink() ), esc_attr( get_the_time() ), esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ) );
	$output .= '<span class="post-meta-value">' . $posted_on . '</span></li>';

	$output .= '<li><span class="post-meta-label">' . __( 'Author', 'dm3_fwk' ) . '</span> ';
	$output .= '<span class="post-meta-value">';
	$output .= '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '" title="' . __( 'View all posts by this author', 'dm3_fwk' ) . '" rel="author">' . esc_attr( get_the_author() ) . '</a></span>';
	$output .= '</span></li> ';

	$categories = get_the_category_list( __( ', ', 'dm3_fwk' ) );

	if ( $categories ) {
		$output .= '<li><span class="post-meta-label">' . __( 'Categories', 'dm3_fwk' ) . '</span> ';
		$output .= '<span class="post-meta-value">' . $categories . '</span></li> ';
	}

	$tags = get_the_tag_list( '', __( ', ', 'dm3_fwk' ) );

	if ( $tags ) {
		$output .= '<li><span class="post-meta-label">' . __( 'Tags', 'dm3_fwk' ) . '</span> ';
		$output .= '<span class="post-meta-value">' . $tags . '</span></li> ';
	}

	if ( comments_open() && ! post_password_required() ) {
		$output .= '<li><span class="post-meta-label">' . __( 'Comments', 'dm3_fwk' ) . '</span> ';
		$output .= '<span class="post-meta-value post-reply-link">';
		ob_start();
		comments_popup_link( __( 'Reply', 'dm3_fwk' ), __( '1 comment', 'dm3_fwk' ), __( '% comments', 'dm3_fwk' ) );
		$output .= ob_get_contents();
		ob_end_clean();
		$output .= '</span></li> ';
	}

	$edit_post_link = get_edit_post_link();

	if ( $edit_post_link ) {
		$output .= '<li><a href="' . esc_url( $edit_post_link ) . '">' . __( 'Edit', 'dm3_fwk' ) . '</a></li>';
	}

	$output .= '</ul></div>';

	return $output;
}
endif;

if ( ! function_exists( 'dm3_page_subtitle' ) ) :
/**
 * Output page sub title.
 *
 * @param array $custom
 * @return string
 */
function dm3_page_subtitle( $custom = array() ) {
	$subtitle = isset( $custom['dm3_fwk_page_subtitle'] ) ? $custom['dm3_fwk_page_subtitle'][0] : '';

	if ( ! $subtitle ) {
		$subtitle = dm3_option( 'page_subtitle', '' );
	}

	if ( $subtitle ) {
		$subtitle = '<div class="page-description">' . esc_html( $subtitle ) . '</div>';
	}

	return $subtitle;
}
endif;

/**
 * Get color schemes.
 *
 * @return array
 */
function dm3_get_colors() {
	return array(
		'blue' => array(
			'primary' => '#4381DE',
			'link_hover' => '#3566B0',
			'header_bg' => '#ffffff',
			'header_text' => '#000000',
			'header_active_bg' => '#eeeeee',
			'btn_bg' => '#4381DE',
			'btn_grad_top' => '#71AAFF',
			'btn_grad_btm' => '#4381DE',
			'btn_border' => '#3465AD',
			'btn_border_top' => '#3E78CF',
			'btn_color' => '#fff',
			'btn_text_shadow' => '#305DA1',
			'btn_box_shadow_in' => '#B0CFFF',
			'btn_box_shadow_out' => '#ddd',
			'btn_hover_border' => '#3465AD',
			'btn_hover_box_shadow_in' => '#4381DE',
			'btn_hover_box_shadow_out' => '#ddd',
			'btn_hover_color' => '#fff'
		),

		'blue2' => array(
			'primary' => '#0689C0',
			'link_hover' => '#056F9C',
			'header_bg' => '#ffffff',
			'header_text' => '#000000',
			'header_active_bg' => '#eeeeee',
			'btn_bg' => '#0689C0',
			'btn_grad_top' => '#08B6FF',
			'btn_grad_btm' => '#0689C0',
			'btn_border' => '#056D99',
			'btn_border_top' => '#0689C0',
			'btn_color' => '#fff',
			'btn_text_shadow' => '#056D99',
			'btn_box_shadow_in' => '#69E1FF',
			'btn_box_shadow_out' => '#eee',
			'btn_hover_border' => '#056D99',
			'btn_hover_box_shadow_in' => '#056D99',
			'btn_hover_box_shadow_out' => '#eee',
			'btn_hover_color' => '#fff'
		),

		'gold' => array(
			'primary' => '#ffb501',
			'link_hover' => '#C98F01',
			'header_bg' => '#ffffff',
			'header_text' => '#000000',
			'header_active_bg' => '#eeeeee',
			'btn_bg' => '#ffb501',
			'btn_grad_top' => '#ffdf30',
			'btn_grad_btm' => '#ffb501',
			'btn_border' => '#d2b700',
			'btn_border_top' => '#e4c600',
			'btn_color' => '#fff',
			'btn_text_shadow' => '#d1b601',
			'btn_box_shadow_in' => '#fff7cd',
			'btn_box_shadow_out' => '#ddd',
			'btn_hover_border' => '#d2b700',
			'btn_hover_box_shadow_in' => '#ffb501',
			'btn_hover_box_shadow_out' => '#ddd',
			'btn_hover_color' => '#fff'
		),

		'green' => array(
			'primary' => '#00C770',
			'link_hover' => '#00AB60',
			'header_bg' => '#ffffff',
			'header_text' => '#000000',
			'header_active_bg' => '#eeeeee',
			'btn_bg' => '#00C770',
			'btn_grad_top' => '#00FF8F',
			'btn_grad_btm' => '#00C770',
			'btn_border' => '#00B566',
			'btn_border_top' => '#00C770',
			'btn_color' => '#fff',
			'btn_text_shadow' => '#00B566',
			'btn_box_shadow_in' => '#B6FFDE',
			'btn_box_shadow_out' => '#ddd',
			'btn_hover_border' => '#00B566',
			'btn_hover_box_shadow_in' => '#00B566',
			'btn_hover_box_shadow_out' => '#ddd',
			'btn_hover_color' => '#fff'
		),

		'green2' => array(
			'primary' => '#8dc90e',
			'link_hover' => '#74A60C',
			'header_bg' => '#ffffff',
			'header_text' => '#000000',
			'header_active_bg' => '#eeeeee',
			'btn_bg' => '#8dc90e',
			'btn_grad_top' => '#bcec58',
			'btn_grad_btm' => '#8dc90e',
			'btn_border' => '#7fb60b',
			'btn_border_top' => '#8cc80d',
			'btn_color' => '#fff',
			'btn_text_shadow' => '#7aae0c',
			'btn_box_shadow_in' => '#d4ff79',
			'btn_box_shadow_out' => '#ddd',
			'btn_hover_border' => '#7fb60b',
			'btn_hover_box_shadow_in' => '#8dc90e',
			'btn_hover_box_shadow_out' => '#ddd',
			'btn_hover_color' => '#fff'
		),

		'orange' => array(
			'primary' => '#e46e05',
			'link_hover' => '#B55704',
			'header_bg' => '#ffffff',
			'header_text' => '#000000',
			'header_active_bg' => '#eeeeee',
			'btn_bg' => '#e46e05',
			'btn_grad_top' => '#ffb68a',
			'btn_grad_btm' => '#e46e05',
			'btn_border' => '#e46e05',
			'btn_border_top' => '#ff8e32',
			'btn_color' => '#fff',
			'btn_text_shadow' => '#e46e05',
			'btn_box_shadow_in' => '#ffd5ba',
			'btn_box_shadow_out' => '#ddd',
			'btn_hover_border' => '#e46e05',
			'btn_hover_box_shadow_in' => '#e46e05',
			'btn_hover_box_shadow_out' => '#ddd',
			'btn_hover_color' => '#fff'
		),

		'purple' => array(
			'primary' => '#d16bd2',
			'link_hover' => '#A756A8',
			'header_bg' => '#ffffff',
			'header_text' => '#000000',
			'header_active_bg' => '#eeeeee',
			'btn_bg' => '#d16bd2',
			'btn_grad_top' => '#f7a4ff',
			'btn_grad_btm' => '#d16bd2',
			'btn_border' => '#d16bd2',
			'btn_border_top' => '#ea7deb',
			'btn_color' => '#fff',
			'btn_text_shadow' => '#d16bd2',
			'btn_box_shadow_in' => '#fcd0ff',
			'btn_box_shadow_out' => '#ddd',
			'btn_hover_border' => '#d16bd2',
			'btn_hover_box_shadow_in' => '#d16bd2',
			'btn_hover_box_shadow_out' => '#ddd',
			'btn_hover_color' => '#fff'
		),

		'red' => array(
			'primary' => '#cf332a',
			'link_hover' => '#a12821',
			'header_bg' => '#ffffff',
			'header_text' => '#000000',
			'header_active_bg' => '#eeeeee',
			'btn_bg' => '#CF332A',
			'btn_grad_top' => '#FA685F',
			'btn_grad_btm' => '#CF332A',
			'btn_border' => '#AB2A23',
			'btn_border_top' => '#CF332A',
			'btn_color' => '#fff',
			'btn_text_shadow' => '#85201B',
			'btn_box_shadow_in' => '#F2ADA8',
			'btn_box_shadow_out' => '#ddd',
			'btn_hover_border' => '#9E2720',
			'btn_hover_box_shadow_in' => '#CF332A',
			'btn_hover_box_shadow_out' => '#ddd',
			'btn_hover_color' => '#fff'
		)
	);
}
