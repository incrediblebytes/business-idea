<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Get plugins required by this theme.
 *
 * @return array
 */
function dm3_bi_get_plugins() {
	$plugins = array(
		array(
			'name'     => 'Dm3Options',
			'slug'     => 'dm3-options',
			'source'   => 'https://bitbucket.org/incrediblebytes/dm3-options/get/1.4.0.zip',
			'required' => true,
			'version'  => '1.4.0',
		),

		array(
			'name'     => 'Dm3Shortcodes',
			'slug'     => 'dm3-shortcodes',
			'source'   => 'https://github.com/educatorplugin/dm3-shortcodes/archive/2.2.1.zip',
			'required' => false,
			'version'  => '2.2.1',
		),

		array(
			'name'     => 'Dm3Media',
			'slug'     => 'dm3-media',
			'source'   => 'https://bitbucket.org/incrediblebytes/dm3-media/get/1.4.0.zip',
			'required' => false,
			'version'  => '1.4.0',
		),

		array(
			'name'     => 'Dm3Sidebars',
			'slug'     => 'dm3-sidebars',
			'source'   => 'https://bitbucket.org/incrediblebytes/dm3-sidebars/get/1.4.0.zip',
			'required' => false,
			'version'  => '1.4.0',
		),

		array(
			'name'     => 'Dm3Widgets',
			'slug'     => 'dm3-widgets',
			'source'   => 'https://bitbucket.org/incrediblebytes/dm3-widgets/get/1.2.0.zip',
			'required' => false,
			'version'  => '1.2.0',
		),

		array(
			'name'     => 'Recent Tweets Widget',
			'slug'     => 'recent-tweets-widget',
			'required' => false,
		),
	);

	return apply_filters( 'dm3_bi_get_plugins', $plugins );
}

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once get_template_directory() . '/inc/class-tgm-plugin-activation.php';

function dm3_register_required_plugins() {
	$config = array(
		'id'           => 'tgmpa',
		'default_path' => '',
		'menu'         => 'tgmpa-install-plugins',
		'parent_slug'  => 'themes.php',
		'capability'   => 'edit_theme_options',
		'has_notices'  => true,
		'dismissable'  => false,
		'dismiss_msg'  => '',
		'is_automatic' => true,
		'message'      => '',
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'dm3_fwk' ),
			'menu_title'                      => __( 'Install Plugins', 'dm3_fwk' ),
			'installing'                      => __( 'Installing Plugin: %s', 'dm3_fwk' ), // %s = plugin name.
			'oops'                            => __( 'Something went wrong with the plugin API.', 'dm3_fwk' ),
			'notice_can_install_required'     => _n_noop(
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'dm3_fwk'
			), // %1$s = plugin name(s).
			'notice_can_install_recommended'  => _n_noop(
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'dm3_fwk'
			), // %1$s = plugin name(s).
			'notice_cannot_install'           => _n_noop(
				'Sorry, but you do not have the correct permissions to install the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to install the %1$s plugins.',
				'dm3_fwk'
			), // %1$s = plugin name(s).
			'notice_ask_to_update'            => _n_noop(
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'dm3_fwk'
			), // %1$s = plugin name(s).
			'notice_ask_to_update_maybe'      => _n_noop(
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'dm3_fwk'
			), // %1$s = plugin name(s).
			'notice_cannot_update'            => _n_noop(
				'Sorry, but you do not have the correct permissions to update the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to update the %1$s plugins.',
				'dm3_fwk'
			), // %1$s = plugin name(s).
			'notice_can_activate_required'    => _n_noop(
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'dm3_fwk'
			), // %1$s = plugin name(s).
			'notice_can_activate_recommended' => _n_noop(
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'dm3_fwk'
			), // %1$s = plugin name(s).
			'notice_cannot_activate'          => _n_noop(
				'Sorry, but you do not have the correct permissions to activate the %1$s plugin.',
				'Sorry, but you do not have the correct permissions to activate the %1$s plugins.',
				'dm3_fwk'
			), // %1$s = plugin name(s).
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'dm3_fwk'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'dm3_fwk'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'dm3_fwk'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'dm3_fwk' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'dm3_fwk' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'dm3_fwk' ),
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'dm3_fwk' ),  // %1$s = plugin name(s).
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'dm3_fwk' ),  // %1$s = plugin name(s).
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'dm3_fwk' ), // %s = dashboard link.
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'tgmpa' ),

			'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
		),
	);

	tgmpa( dm3_bi_get_plugins(), $config );
}
add_action( 'tgmpa_register', 'dm3_register_required_plugins' );
