<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$page_bg = dm3_option( 'page_bg' );
$colors = dm3_get_colors();
$default_color = 'blue2';
$selected_color = dm3_option( 'color_scheme', $default_color );
$ajax_loader = dm3_option( 'ajax_loader', 'black' );
$web_font = dm3_option( 'web_font', 'bebas_neueregular' );
$custom_css = '';

if ( isset( $colors[ $selected_color ] ) ) {
	$color = $colors[ $selected_color ];
} else {
	$color = $colors[ $default_color ];
}

foreach ( $color as $c_name => $c_value ) {
	$replace_color = dm3_option( 'c_' . $c_name, '' );

	if ( $replace_color ) {
		$color[ $c_name ] = esc_html( $replace_color );
	}
}

// Font.
switch ( $web_font ) {
	case 'Oswald':
		$font_family = 'Oswald';
		wp_enqueue_style( 'Oswald', $protocol . '://fonts.googleapis.com/css?family=Oswald:400,700' );
		$custom_css .= '.nav-desktop > li > a {font-size: 17px;}';
		break;

	case 'Roboto Condensed':
		$font_family = 'Roboto Condensed';
		wp_enqueue_style( 'Roboto Condensed', $protocol . '://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&subset=latin,cyrillic' );
		$custom_css .= '.nav-desktop > li > a {font-size: 18px;}';
		break;

	case 'Yanone Kaffeesatz':
		$font_family = 'Yanone Kaffeesatz';
		wp_enqueue_style( 'Roboto Condensed', $protocol . '://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,700' );
		break;

	case 'Lobster':
		$font_family = 'Lobster';
		wp_enqueue_style( 'Lobster', $protocol . '://fonts.googleapis.com/css?family=Lobster&subset=latin,cyrillic' );
		$custom_css .= '.nav-desktop > li > a {font-size: 18px; padding-left: 16px; padding-right: 16px;}';
		break;
	
	default:
		$font_family = 'bebas_neueregular';
		$custom_css .= '
@font-face {
	font-family: "bebas_neueregular";
	src: url("' . $template_url . '/css/fonts/BebasNeue-webfont.eot");
	src: url("' . $template_url . '/css/fonts/BebasNeue-webfont.eot?#iefix") format("embedded-opentype"),
			 url("' . $template_url . '/css/fonts/BebasNeue-webfont.woff") format("woff"),
			 url("' . $template_url . '/css/fonts/BebasNeue-webfont.ttf") format("truetype"),
			 url("' . $template_url . '/css/fonts/BebasNeue-webfont.svg#bebas_neueregular") format("svg");
	font-weight: bold;
	font-style: normal;
}
';
		break;
}

$site_bg_css = '';

if ( ! empty( $page_bg ) ) {
	$site_bg_css = '#site-bg {background-image: url("' . esc_url( $page_bg ) . '");}';
}

$custom_css .= '
h1, h2, h3, h4, h5, h6, .nav-desktop > li > a, #nav-mobile > li > a {
	font-family: "' . $font_family . '", Arial, Helvetica, sans-serif;
	font-weight: bold;
}

' . $site_bg_css . '

a {
	color: ' . $color['primary'] . ';
	-webkit-transition: color 0.2s;
	transition: color 0.2s;
}

a:hover {
	color: ' . $color['link_hover'] . ';
}

.nav-desktop .current-menu-item > a,
.nav-desktop .current-menu-ancestor > a,
#search-trigger.active span,
.dm3-member-social a:hover,
.post-meta a:hover,
.post-short .post-header a:hover,
.sidebar li a:hover,
.comment-meta a:hover,
.comment-reply-link:hover,
.dm3-box-icon-left .dm3-box-icon-icon span,
.flexslider-posts .description h2 a:hover,
.dm3-box-icon-center .dm3-box-icon-icon span {
	color: ' . $color['primary'] . ';
}

#site-nav,
.nav-desktop ul {
	border-top-color: ' . $color['primary'] . ';
}

#nav-pointer {
	border-color: ' . $color['primary'] . ' transparent transparent transparent;
}

.flex-prev span {
	border-color: transparent ' . $color['primary'] . ' transparent transparent;
}

.flex-next span {
	border-color: transparent transparent transparent ' . $color['primary'] . ';
}

#header-toolbar,
body .flex-control-nav .flex-active,
.dm3-gallery-popover .icon,
.dm3-tabs-testimonials .dm3-tabs-nav .active a,
.dm3-widgets-contacts .icon > div,
#header-search button {
	background-color: ' . $color['primary'] . ';
}

.dm3-gallery-terms a:hover,
.dm3-gallery-terms .active a,
.posts-navigation a:hover,
.pager > a:hover,
.pager > span,
.page-links a:hover {
	color: ' . $color['primary'] . ';
	border-color: ' . $color['primary'] . ';
}

#site-nav {
	background-color: ' . $color['header_bg'] . ';
}

.nav-desktop .current-menu-item > a,
.nav-desktop .current-menu-ancestor > a,
#search-trigger.active {
	background-color: ' . $color['header_active_bg'] . ';
}

.nav-desktop > li > a,
#search-trigger span {
	color: ' . $color['header_text'] . ';
}

/* Buttons */
.dm3-btn-primary,
input#submit,
.wpcf7-submit {
	background-color: ' . $color['btn_bg'] . ';
	background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, ' . $color['btn_grad_top'] . '), color-stop(100%, ' . $color['btn_grad_btm'] . '));
	background-image: -moz-linear-gradient(top, ' . $color['btn_grad_top'] . ' 0%, ' . $color['btn_grad_btm'] . ' 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr="' . $color['btn_grad_top'] . '", endColorstr="' . $color['btn_grad_btm'] . '",GradientType=0);
	border: 1px solid ' . $color['btn_border'] . ';
	border-top: 1px solid ' . $color['btn_border_top'] . ';
	color: ' . $color['btn_color'] . ';
	text-shadow: 0 -1px 1px ' . $color['btn_text_shadow'] . ';
	box-shadow: inset 0 1px 0 0 ' . $color['btn_box_shadow_in'] . ', 0 1px 5px ' . $color['btn_box_shadow_out'] . ';
}

.dm3-btn-primary:active,
input#submit:active,
.wpcf7-submit:active {
	border: 1px solid ' . $color['btn_hover_border'] . ';
	box-shadow: inset 0 0 8px 2px ' . $color['btn_hover_box_shadow_in'] . ', 0 1px 0 0 ' . $color['btn_hover_box_shadow_out'] . ';
}

.dm3-btn-primary:hover {
	color: ' . $color['btn_hover_color'] . ';
}
';

if ( $ajax_loader == 'white' ) {
	$custom_css .= '
.content-inner > .content-loader, #ajax-preloader { background-image: url("' . $template_url . '/images/preloader-white.gif"); }
@media only screen and (max-width: 767px) {
#ajax-preloader { background-image: url("' . $template_url . '/images/preloader-white-small.gif"); }
}
';
}

wp_add_inline_style( 'style', $custom_css );
