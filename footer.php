				<!-- Footer -->
				<section id="footer">
					<div class="container clearfix">
						<?php
							if ( dm3_option( 'show_footer_widgets', 1 ) ) {
								$layout = dm3_option( 'footer_layout', '' );

								switch ( $layout ) {
									case '1/2 1/2':
										?>
										<div id="footer-widgets" class="clearfix">
											<div class="eight columns">
												<?php dynamic_sidebar( 'theme-footer-1' ); ?>
											</div>

											<div class="eight columns">
												<?php dynamic_sidebar( 'theme-footer-2' ); ?>
											</div>
										</div>
										<?php
										break;

									case '1/4 1/4 1/2':
										?>
										<div id="footer-widgets" class="clearfix">
											<div class="four columns">
												<?php dynamic_sidebar( 'theme-footer-1' ); ?>
											</div>

											<div class="four columns">
												<?php dynamic_sidebar( 'theme-footer-2' ); ?>
											</div>

											<div class="eight columns">
												<?php dynamic_sidebar( 'theme-footer-3' ); ?>
											</div>
										</div>
										<?php
										break;

									default:
										?>
										<div id="footer-widgets" class="clearfix">
											<div class="three columns">
												<?php dynamic_sidebar( 'theme-footer-1' ); ?>
											</div>

											<div class="three columns">
												<?php dynamic_sidebar( 'theme-footer-2' ); ?>
											</div>

											<div class="three columns">
												<?php dynamic_sidebar( 'theme-footer-3' ); ?>
											</div>

											<div class="five columns offset-by-two">
												<?php dynamic_sidebar( 'theme-footer-4' ); ?>
											</div>
										</div>
										<?php
										break;
								}
							}
						?>
						<div id="footer-bottom" class="clearfix">
							<div id="footer-copy">
								<?php echo dm3_option( 'copyright', 'Footer copyright notice' ); ?>
							</div>

							<a id="footer-back-to-top" href="#">&uarr; <?php _e( 'Top', 'dm3_fwk' ); ?></a>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</section>

<?php wp_footer(); ?>
</body>
</html>
