<?php

if ( ! defined( 'ABSPATH' ) ) exit;

define( 'DM3THEME', 'business-idea' );
define( 'DM3THEME_NAME', 'Business Idea' );
define( 'DM3THEME_VER', '1.8.0' );

if ( ! isset( $content_width ) ) {
	$content_width = 940;
}

require_once get_template_directory() . '/inc/functions-base.php';

if ( is_admin() && current_user_can( 'install_plugins' ) ) {
	require_once get_template_directory() . '/inc/plugins.php';
}

/**
 * Setup theme features.
 */
function dm3_theme_setup() {
	if ( is_admin() ) {
		add_editor_style();
	}

	// Add image sizes.
	$image_sizes = dm3_get_img_sizes();

	foreach ( $image_sizes as $name => $size ) {
		add_image_size( $name, $size['w'], $size['h'], true );
	}

	// Load text domain.
	load_theme_textdomain( 'dm3_fwk', get_template_directory() . '/languages' );

	$locale_file = get_template_directory() . '/languages/' . get_locale() . '.php';

	if ( file_exists( $locale_file ) ) {
		require_once $locale_file;
	}

	// Custom menus.
	register_nav_menus( array(
		'primary' => __( 'Main Menu', 'dm3_fwk' ),
	) );

	// Adds support for posts thumbnails.
	add_theme_support( 'post-thumbnails' );
	
	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
	
	// Adds support for excerpts in pages.
	add_post_type_support( 'page', 'excerpt' );
	
	// Allow shortcodes in widgets.
	add_filter( 'widget_text', 'shortcode_unautop' );
	add_filter( 'widget_text', 'do_shortcode' );
}
add_action( 'after_setup_theme', 'dm3_theme_setup' );

/**
 * Register custom post types and taxonomies.
 */
function dm3_register_post_types() {
	// Custom post type: gallery.
	register_post_type( 'gallery', array(
		'label'              => __( 'Gallery', 'dm3_fwk' ),
		'singular_label'     => __( 'Gallery Item', 'dm3_fwk' ),
		'capability_type'    => 'post',
		'publicly_queryable' => true,
		'has_archive'        => false,
		'show_ui'            => true,
		'supports'           => array( 'title', 'editor', 'thumbnail', 'excerpt', 'page-attributes' ),
		'taxonomies'         => array( 'gallery_cat' ),
		'rewrite'            => array( 'slug' => dm3_option( 'gallery_slug', 'gallery' ) ),
	) );

	// Register taxonomy for gallery posts.
	register_taxonomy( 'gallery_cat', 'gallery', array(
		'label'   => __( 'Gallery categories', 'dm3_fwk' ),
		'public'  => false,
		'show_ui' => true,
	) );
}
add_action( 'init', 'dm3_register_post_types' );

/**
 * Add slideshow to custom post types.
 *
 * @param array $post_types
 * @return array
 */
function dm3_slideshow_post_types( $post_types ) {
	$post_types[] = 'gallery';

	return $post_types;
}
add_filter( 'dm3media_post_types', 'dm3_slideshow_post_types' );

/**
 * Enqueue scripts and styles.
 */
function dm3_enqueue_scripts_styles() {
	$template_url = get_template_directory_uri();

	// Scripts.
	wp_enqueue_script( 'dm3-easing', $template_url . '/js/jquery.easing.js', array( 'jquery' ), '1.3' );
	wp_enqueue_script( 'dm3-flexslider', $template_url . '/js/jquery.flexslider.js', array( 'jquery' ), '2.4.0', true );
	wp_enqueue_script( 'dm3-dm3Rs', $template_url . '/js/jquery.dm3Rs.js', array( 'jquery' ), '1.3.1', true );
	wp_enqueue_script( 'dm3-imagesloaded', $template_url . '/js/imagesloaded.js', array(), '3.1.4', true );
	wp_enqueue_script( 'dm3-isotope', $template_url . '/js/isotope.min.js', array( 'jquery' ), '2.0.0', true );
	wp_enqueue_script( 'dm3-magnificPopup', $template_url . '/js/jquery.magnificPopup.js', array( 'jquery' ), '1.0.0', true );
	wp_enqueue_script( 'dm3-website', $template_url . '/js/website.js', array( 'jquery' ), '1.7', true );
	wp_enqueue_script( 'dm3-page', $template_url . '/js/page.js', array( 'jquery' ), '1.7', true );

	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_localize_script( 'dm3-website', 'dm3Theme', array(
		'baseUrl' => esc_url_raw( home_url( '/' ) ),
	) );

	// Dequeue dm3-shortcodes plugin icons.
	wp_dequeue_style( 'font-awesome' );

	// Custom shortcodes styles.
	if ( function_exists( 'dm3sc_init' ) ) {
		wp_enqueue_style( 'dm3-custom-shortcodes', $template_url . '/css/custom-shortcodes.css', array( 'dm3-shortcodes-front' ), '1.0' );
		wp_dequeue_script( 'dm3-shortcodes-front' );
		wp_enqueue_script( 'dm3-custom-shortcodes', $template_url . '/js/custom-shortcodes.js', array( 'jquery' ), '1.0' );
	}

	// Dequeue recent-tweets-widget default styles.
	if ( function_exists( 'tp_twitter_plugin_styles' ) ) {
		wp_dequeue_style( 'tp_twitter_plugin_css' );
	}
	
	// Styles.
	wp_enqueue_style( 'dm3-reset', $template_url . '/css/reset.css' );
	wp_enqueue_style( 'dm3-skeleton', $template_url . '/css/skeleton.css' );
	wp_enqueue_style( 'dm3-fonts', $template_url . '/css/fonts.css' );
	wp_enqueue_style( 'dm3-flexslider', $template_url . '/css/flexslider.css' );
	wp_enqueue_style( 'dm3-magnificPopup', $template_url . '/css/magnificPopup.css' );
	wp_enqueue_style( 'style', get_stylesheet_uri() );

	require_once get_template_directory() . '/css/color.php';
}
add_action( 'wp_enqueue_scripts', 'dm3_enqueue_scripts_styles', 11 );

/**
 * Custom image sizes in media library.
 *
 * @param array $sizes
 * @return array
 */
function dm3_custom_image_sizes_choose( $sizes ) {
	$img_size = dm3_get_img_sizes();
	
	foreach ( $img_size as $key => $data ) {
		$sizes[$key] = $data['name'];
	}
	
	return $sizes;
}
add_filter( 'image_size_names_choose', 'dm3_custom_image_sizes_choose' );

/**
 * Creates a nicely formatted and more specific title element text
 * for output in head of document, based on current view.
 *
 * @param string $title
 * @param string $sep
 * @return string
 */
function dm3_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );

	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'dm3_fwk' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'dm3_wp_title', 10, 2 );

/**
 * Sidebars and widgets.
 */
function dm3_widgets_init() {
	// Register sidebar: default.
	register_sidebar( array(
		'name'          => __( 'Default Theme Sidebar', 'dm3_fwk' ),
		'id'            => 'default-theme-sidebar',
		'description'   => null,
		'before_widget' => '<div class="widget">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
 
	// Register sidebar: footer.
	$layout = dm3_option( 'footer_layout', '' );

	switch ( $layout ) {
		case '1/2 1/2':
			$num_columns = 2;
			break;

		case '1/4 1/4 1/2':
			$num_columns = 3;
			break;

		default:
			$num_columns = 4;
	}

	for ( $i = 1; $i <= $num_columns; $i++ ) {
		register_sidebar( array(
			'name'          => sprintf( __( 'Footer %d', 'dm3_fwk' ), $i ),
			'id'            => 'theme-footer-' . $i,
			'description'   => null,
			'before_widget' => '<div class="widget">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3 class="widget-title">',
			'after_title'   => '</h3>',
		) );
	}
}
add_action( 'widgets_init', 'dm3_widgets_init' );

/**
 * Dm3Sidebars configure arguments.
 *
 * @param array $args
 * @return array
 */
function dm3_custom_sidebar_args( $args ) {
	return $args;
}
add_filter( 'dm3sb_sidebar_args', 'dm3_custom_sidebar_args' );

/**
 * Dm3Sidebars set post types.
 *
 * @param array $post_types
 * @return array
 */
function dm3_custom_sidebar_post_types( $post_types ) {
	return $post_types;
}
add_filter( 'dm3sb_post_types', 'dm3_custom_sidebar_post_types' );

/**
 * Excerpt length filter.
 *
 * @param int $length
 * @return int
 */
function dm3_excerpt_length( $length ) {
	return 15;
}
add_filter( 'excerpt_length', 'dm3_excerpt_length' );

/**
 * Excerpt more text filter.
 *
 * @param string $more
 * @return string
 */
function dm3_excerpt_more( $more ) {
	return '&hellip;';
}
add_filter( 'excerpt_more', 'dm3_excerpt_more' );

/**
 * Nav menu link attributes filter.
 *
 * @param array $atts
 * @param Object $item
 * @param Object $args
 * @return array
 */
function dm3_add_submenu_link_class( $atts, $item, $args ) {
	if ( $args->theme_location == 'primary' ) {
		if ( ! isset( $atts['class'] ) ) {
			$atts['class'] = 'ajax-link';
		} else {
			$atts['class'] .= ' ajax-link';
		}
	}

	return $atts;
}
add_filter( 'nav_menu_link_attributes', 'dm3_add_submenu_link_class', 10, 3 );

/**
 * Contacts widget filter.
 *
 * @param string $tr Table row.
 * @param string $type
 * @return string
 */
function dm3_contacts_widget_tr( $tr, $type ) {
	if ( $type == 'email' ) {
		$icon = '<span class="font-icon-envelope"></span>';
	} else if ( $type == 'phone' ) {
		$icon = '<span class="font-icon-phone"></span>';
	} else {
		$icon = '<span class="font-icon-map-marker"></span>';
	}

	return str_replace( 'icon">', 'icon"><div>' . $icon . '</div>', $tr );
}
add_filter( 'Dm3WidgetsContacts_tr', 'dm3_contacts_widget_tr', 10, 2 );

/**
 * Posts carousel shortcode filter.
 *
 * @param string $value
 * @param string $filter
 * @return string
 */
function dm3_posts_carousel_filter( $value, $filter ) {
	switch ( $filter ) {
		case 'before posts':
			return '<div class="flexslider flexslider-carousel flexslider-posts"><ul class="slides">';

		case 'after posts':
			return '</ul></div>';
	}

	return $value;
}
add_filter( 'dm3sc_posts_carousel', 'dm3_posts_carousel_filter', 10, 2 );

/**
 * Clients carousel shortcode filter.
 *
 * @param string $value
 * @param string $filter
 * @return string
 */
function dm3_clients_carousel_filter( $value, $filter ) {
	switch ( $filter ) {
		case 'before':
			return '<div class="flexslider flexslider-carousel flexslider-logos"><ul class="slides">';

		case 'after':
			return '</ul></div>';
	}

	return $value;
}
add_filter( 'dm3sc_clients_carousel', 'dm3_clients_carousel_filter', 10, 2 );

/**
 * Set the icon font to use with the Dm3Shortcodes plugin.
 *
 * @param string $font
 * @return string
 */
function dm3_bi_icon_font( $font ) {
	return 'font-awesome-3.2.1';
}
add_filter( 'dm3_shortcodes_icon_font', 'dm3_bi_icon_font' );

/**
 * Add google maps if ajax enabled.
 */
function dm3_ajax_enabled_init() {
	// Only for ajax enabled.
	if ( ! dm3_option( 'enable_ajax', 0 ) ) {
		return;
	}

	wp_enqueue_script( 'google-maps', '//maps.google.com/maps/api/js?sensor=false' );
}
add_action( 'init', 'dm3_ajax_enabled_init' );

if ( function_exists( 'dm3sc_init' ) ) {
	require get_template_directory() . '/inc/custom-shortcodes.php';
}

/**
 * Remove requests to wp.org repository for this theme.
 *
 * @param array $r
 * @param string $url
 * @return array
 */
function bi_prevent_wp_org_update_request( $r, $url ) {
	if ( 0 !== strpos( $url, 'https://api.wordpress.org/themes/update-check' ) ) {
		return $r;
	}

	if ( empty( $r['body']['themes'] ) ) {
		return $r;
	}

	$themes = json_decode( $r['body']['themes'] );

	if ( ! is_object( $themes ) ) {
		return $r;
	}

	$parent = get_option( 'template' );
	$child = get_option( 'stylesheet' );

	unset( $themes->themes->$parent );

	if ( $child != $parent ) {
		unset( $themes->themes->$child );
	}

	$r['body']['themes'] = json_encode( $themes );

	return $r;
}
add_filter( 'http_request_args', 'bi_prevent_wp_org_update_request', 5, 2 );
